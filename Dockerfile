FROM python:3.9

WORKDIR /code-server

# requirements
RUN apt-get update && apt-get install -y gdebi

# install
RUN wget https://github.com/coder/code-server/releases/download/v4.5.0/code-server_4.5.0_amd64.deb
RUN gdebi --non-interactive code-server_4.5.0_amd64.deb

CMD code-server --bind-addr "0.0.0.0:8080" --auth none
