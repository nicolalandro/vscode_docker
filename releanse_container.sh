#!/usr/bin/env bash

#docker login registry.gitlab.com
docker login registry.gitlab.com -u nicolalandro

docker build -t registry.gitlab.com/nicolalandro/vscode_docker .
docker push registry.gitlab.com/nicolalandro/vscode_docker
