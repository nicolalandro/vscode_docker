# VSCode_Docker

The aim of this codebase it to create a docker container with vscode for python.

# use it

(remember to use sudo if you do not add your user to docker group)
```
docker pull registry.gitlab.com/nicolalandro/vscode_docker
docker run -it --rm -p 8080:8080 registry.gitlab.com/nicolalandro/vscode_docker

firefox localhost:8080
```

![](imgs/screen1.png)

## Build and deploy
(remember to use sudo if you do not add your user to docker group)

```
# login
docker login registry.example.com -u <username>

build and deploy
./release_container.sh

```

# References

* [vscode](https://code.visualstudio.com/)
* [code-server](https://github.com/cdr/code-server)
* Doker
* [Gitlab Container Registry](https://docs.gitlab.com/ee/user/packages/container_registry/)
